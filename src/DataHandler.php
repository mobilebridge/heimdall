<?php

namespace Heimdall;

use Closure;
use DateTime;
use InvalidArgumentException;
use Heimdall\Objects\AbstractObject;
use Heimdall\Repositories\RepositoryInterface;


Class DataHandler {	

	protected $repository;

	protected $objects = array();

	protected $extended = array();

	protected $types = array('group', 'rule', 'permission');

	public function __construct(RepositoryInterface $repository)
	{
		$this->repository = $repository;		
	}
	
	public function setOptions(array $options)
	{
		$this->options = $options;
	}

	public function getOption($key)
	{
		return isset($this->options[$key]) ? $this->options[$key] : null;
	}

	protected function getCachedObject($cache, $id)
	{
		$cache = strtolower($cache);

		if(isset($this->objects[$cache]) && isset($this->objects[$cache][$id])) 
		{
			$cache = $this->objects[$cache];
			
			return $cache[$id];
		}
	}

	public function getPermissionRule($id)
	{
		$rule = $this->repository->getPermissionRule($id);

		return $this->getRule($rule);
	}

	public function getGroupPermissions($id)
	{
		$permissions = $this->repository->getGroupPermissions($id);

		foreach($permissions as &$permission)
		{
			$permission = $this->getPermission($permission);
		}

		return $permissions;
	}

	public function getRulePermissions($id)
	{
		$permissions = $this->repository->getRulePermissions($id);

		foreach($permissions as &$permission)
		{
			$permission = $this->getPermission($permission);
		}

		return $permissions;
	}

	public function getGroupParent($id)
	{
		$parent = $this->repository->getGroupParent($id);
		
		return $parent ? $this->getGroup($parent) : null;
	}

	public function getGroupChildren($id)
	{
		$children = $this->repository->getGroupChildren($id);

		$children = $children->map(function($child)
		{
			return $this->getGroup($child['id']);
		});

		$out = array();

		foreach($children->all() as $child)
		{
			if($child) $out[] = $child;
		}

		return $out;
	}

	protected function setCachedObject($cache, $object)
	{
		$cache = strtolower($cache);

		if(! isset($this->objects[$cache])) $this->objects[$cache] = array();

		$id = $object->getIdentifier();

		$this->objects[$cache][$id] = $object;
	}

	protected function createObject($type, $attributes)
	{
		$class = 'Heimdall\\Objects\\' . ucfirst($type);

		return new $class($attributes, $this);
	}

	protected function getObject($type, $id)
	{
		if($object = $this->getCachedObject($type, $id)) 
		{
			return $object;
		}

		$attributes = $this->repository->{'get' . ucfirst($type)}($id);

		if(empty($attributes)) return null;
		
		$object = $this->createObject($type, $attributes);

		$this->setCachedObject($type, $object);

		return $object;
	}

	protected function getObjects($type)
	{
		$all = $this->repository->{'get' . ucfirst($type) . 's'}();

		$out = array();

		foreach($all as $item)
		{
			if(! ($object = $this->getCachedObject($type, $item['id']))) 
			{
				$object = $this->createObject($type, $item);

				$this->setCachedObject($type, $object);
			}

			$out[] = $object;
		}

		return $out;
	}

	public function castValue($value, $type)
	{
		$type = strtolower($type);

		switch($type)
		{
			case 'boolean':
			case 'bool':
				$value = strtolower($value);
				if(is_numeric($value) && (bool) $value === true ) $value = 'true';
				return $value === 'true'; 
			break;
			case 'int':
			case 'integer':
				return (int) $value;
			break;	
			case 'date':
			case 'datetime':
				return new DateTime($value);
			break;
		}

		if(isset($this->extended[$type]))
		{
			return $this->extended[$type]($value);
		}

		throw new InvalidArgumentException('Value type [' . $type . '] is not supported');
	}

	public function extend($type, Closure $caster)
	{
		$this->extend[$type] = $caster;

		return $this;
	}

	public function __call($method, $arguments)
	{
		foreach($this->types as $type)
		{
			$call = 'get' . ucfirst($type);

			if($method === $call)
			{
				return $this->getObject($type, $arguments[0]);
			}
			else if($method === ($call . 's'))
			{
				return $this->getObjects($type);
			}
		}

		throw new \BadMethodCallException($method);
	}
}