<?php

namespace Heimdall;

use Illuminate\Support\Collection;
use Heimdall\Repositories\RepositoryInterface;

Class Heimdall {

	protected $handler;

	public function __construct(RepositoryInterface $repository)
	{
		$this->handler = new DataHandler($repository);
	}

	public function getPermissionsByGroups(array $groups, $inherited = false, $attribute = 'id')
	{
		$rules = array();

		$groups = $this->getGroupsByArray($groups);

		foreach($groups as $group) 
		{
			$found = $group->getPermissionsByRuleKey($attribute, $inherited);

			if($found) $rules[] = $found;
		}

		if(! count($rules)) return new Collection;

		$rules = call_user_func_array('array_merge', $rules);

		return new Collection($rules);
	}

	public function castPermissionValue(Permission $permission)
	{
		return $this->castValue($permission->get('value'), $permission->getRule()->get('type'));
	}

	public function getGroupsByArray(array $ids)
	{
		$out = array();

		foreach($ids as $id)
		{
			$group = $this->handler->getGroup($id);

			if($group) $out[] = $group;
		}

		return $out;
	}

	public function __call($method, $arguments)
	{
		return call_user_func_array(array($this->handler, $method), $arguments);
	}
}
