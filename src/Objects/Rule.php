<?php

namespace Heimdall\Objects;

use Illuminate\Support\Collection;

Class Rule extends AbstractObject {

	public function getPermissions()
	{
		$permissions = $this->handler->getRulePermissions($this->getIdentifier());

		return new Collection($permissions);
	}

	public function getIdentifier()
	{
		return $this->attributes['slug'];
	}

	public function getType()
	{
		return $this->get('type');
	}

	public function getValue()
	{
		return $this->handler->castValue($this->get('value'), $this->getType());
	}
}