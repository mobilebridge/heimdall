<?php

namespace Heimdall\Objects;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

Class Group extends AbstractObject {

	public function getRules()
	{
		$permissions = $this->getPermissions();

		return $permissions->map(function($permission)
		{
			return $permission->getRule();
		});
	}

	public function getPermissions()
	{
		$groups = $this->handler->getGroupPermissions($this->getIdentifier());

		return new Collection($groups);
	}

	public function getAncestors()
	{
		$groups = array();

		$parent = $this;

		do {
			$parent = $parent->getParentGroup();

			if($parent) $groups[] = $parent;
		}
		while($parent);

		return new Collection(array_reverse($groups));
	}

	

	public function getPermissionsByRuleKey($attribute, $inherited = false)
	{
		$out = array();

		$permissions = $inherited
			? $this->getInheritedPermissions() 
			: $this->getPermissions();

		foreach($permissions as $permission)
		{
			$key = $permission->getRule()->get($attribute);

			$out[$key] = $permission;
		}

		return $out;
	}	

	public function getPermissionByRuleKey($attribute, $value, $inherited = true)
	{
		$permissions = $inherited
			? $this->getInheritedPermissions() 
			: $this->getPermissions();

		foreach($permissions as $permission)
		{
			if($permission->getRule()->get($attribute) == $value)
			{
				return $permission;
			}
		}
	}

	public function getPermission($slug, $inherited = false)
	{
		$permissions = $this->getPermissionsByRuleKey('slug', $inherited);
		
		if( $permissions && $permissions[$slug]->get('value') === 'true' ) return true;

		return false;
	}

	public function children($depth = null)
	{
		$out = array();

		$children = $this->handler->getGroupChildren($this->getIdentifier());

		if(! $depth) return $children;

		$out = $children;

		$doTheThing = $depth === true;

		for($i = 0; $i < $depth; ++$i)
		{
			if(! count($children)) break;

			$next = array();

			foreach($children as $child)
			{
				$next = array_merge($next, $child->children());
			}

			$out = array_merge($out, $next);

			$children = $next;

			if($doTheThing) --$i;
		}

		return  $out;
	}

	public function getInheritedPermissions()
	{
		$out = array();

		foreach($this->getAncestors() as $group)
		{
			$permissions = $group->getPermissions()->keyBy(function($permission)
			{
				return $permission->getRule()->getIdentifier();
			})->all();

			$out = array_merge($out, $permissions);
		}

		$permission = Collection::make($this->getPermissions())->keyBy(function($permission)
			{
				return $permission->getRule()->getIdentifier();
		})->all();

		$out = array_merge($out, $permission);

		return new Collection(array_values($out));
	}

	public function getParentGroup()
	{
		return $this->handler->getGroupParent($this->getIdentifier());
	}

	public function getDepth()
	{
		return count($this->getAncestors());
	}

}