<?php

namespace Heimdall\Objects;

Class Permission extends AbstractObject {
	
	public function getRule()
	{
		return $this->handler->getPermissionRule($this->getIdentifier());
	}

	public function getGroup()
	{
		return $this->handler->getPermissionGroup($this->getIdentifier());
	}

	public function getValue()
	{
		$type = $this->getRule()->getType();

		return $this->handler->castValue($this->get('value'), $type);
	}
}