<?php

namespace Heimdall\Objects;

use Heimdall\DataHandler;
use Illuminate\Contracts\Support\Arrayable;

Abstract class AbstractObject implements Arrayable {
	
	protected $handler;

	protected $attributes = array();

	public function __construct(array $attributes = null, DataHandler $handler)
	{
		if( $attributes ) $this->fill($attributes);

		$this->handler = $handler;
	}

	public function toArray()
	{
		return $this->attributes;
	}

	public function getIdentifier()
	{
		return $this->attributes['id'];
	}

	public function setDataHandler($handler)
	{
		$this->handler = $handler;
	}

	public function fill(array $attributes)
	{
		$this->attributes = array_merge($this->attributes, $attributes);
	}

	public function get($attribute)
	{
		return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : null;	
	}

	public function __get($key)
	{
		return $this->get($key);
	}

	public function __isset($key)
	{
		return ! is_null($this->attributes[$key]);
	}

}