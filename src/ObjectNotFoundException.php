<?php

namespace Heimdall;

Class ObjectNotFoundException extends \Exception {

	public function setObject($type, $id)
	{
		$msg = ucfirst($type) . " [$id] is not found";

		$this->message = $msg;
	}
}