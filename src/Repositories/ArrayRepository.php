<?php

namespace Heimdall\Repositories;

use Illuminate\Support\Collection;
use Heimdall\ObjectNotFoundException;

Class ArrayRepository implements RepositoryInterface {
	
	protected $rules;

	protected $groups;

	protected $permissions;

	protected $options = array(
		'permissionGroupParentIdKey' => 'parent_id',
		'permissionGroupIdKey' => 'group_id',
		'permissionRuleIdKey' => 'rule_slug'
	);

	public function __construct(array $groups, array $permissions, array $rules)
	{
		$this->rules = $rules;

		$this->groups = $groups;

		$this->permissions = $permissions;
	}

	public function setOptions(array $options)
	{
		$this->options = $options;

		return $this;
	}

	public function getRule($id)
	{
		return $this->find($this->getRules(), 'slug', $id);
	}

	public function getRulesOrFail($id)
	{
		$found = $this->getRules($id);

		if($found) return $found;

		$err = new ObjectNotFoundException;

		$err->setObject($matches[1], $arguments[0]);

		throw $err;
	}	

	public function getPermissionOrFail($slug)
	{
		$found = $this->getPermission($slug);

		if($found) return $found;

		$err = new ObjectNotFoundException;

		$err->setObject($matches[1], $arguments[0]);

		throw $err;
	}

	public function getGroupOrFail($id)
	{
		$found = $this->getGroup($id);

		if($found) return $found;

		$err = new ObjectNotFoundException;

		$err->setObject($matches[1], $arguments[0]);

		throw $err;
	}

	public function getGroup($id)
	{
		return $this->find($this->getGroups(), 'id', $id);
	}

	public function getPermission($id)
	{
		return $this->find($this->getPermissions(), 'id', $id);
	}

	public function getGroupParent($id)
	{
		$group = $this->getGroupOrFail($id);

		$key = $this->options['permissionGroupParentIdKey'];
		
		return $group[$key] ?: null;
	}

	public function getGroupChildren($id)
	{
		$group = $this->getGroupOrFail($id);

		$key = $this->options['permissionGroupParentIdKey'];
		
		return $this->where($this->getGroups(), $key, $id);
	}

	public function getRulePermissions($id)
	{
		$key = $this->options['permissionRuleIdKey'];

		return $this->where($this->getPermissions(), $key, $id)->lists('id');
	}

	public function getPermissionRule($id)
	{
		$permission = $this->getPermissionOrFail($id);

		$key = $this->options['permissionRuleIdKey'];
		
		return $permission[$key];
	}

	public function getPermissionGroup($id)
	{
		$permission = $this->getPermissionOrFail($id);

		$key = $this->options['permissionGroupIdKey'];

		return $this->getGroup($permission[$key]);
	}

	public function getGroupPermissions($id)
	{
		$permissions = new Collection($this->getPermissions());

		$key = $this->options['permissionGroupIdKey'];

		return $permissions->filter(function($permission) use($id, $key)
		{
			return $permission[$key] === $id;
		})->lists('id');
	}

	protected function where(array $items, $key, $value, $strict = true)
	{
		$collection = new Collection($items);

		return $collection->where($key, $value, $strict);
	}

	protected function find(array $items, $key, $value, $strict = true)
	{
		foreach($items as $item)
		{
			if($item[$key] === $value) return $item;
		}
	}

	public function getGroups()
	{
		return $this->groups;
	}

	public function getPermissions()
	{
		return $this->permissions;
	}

	public function getRules()
	{
		return $this->rules;
	}


}